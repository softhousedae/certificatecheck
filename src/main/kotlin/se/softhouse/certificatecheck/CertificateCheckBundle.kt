package se.softhouse.certificatecheck

import io.dropwizard.ConfiguredBundle
import io.dropwizard.client.HttpClientConfiguration
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import javax.validation.Valid
import javax.validation.constraints.NotNull

data class CertificateCheckSettings(@Valid
                                    @NotNull
                                    val httpClient: HttpClientConfiguration = HttpClientConfiguration())

interface CertificateCheckConfig {
    var certificateCheckSettings: CertificateCheckSettings
}

class CertificateCheckBundle<C : CertificateCheckConfig> : ConfiguredBundle<C> {

    lateinit var component: CertificateCheckComponent

    override fun initialize(bootstrap: Bootstrap<*>) {
        // deliberately empty
    }

    override fun run(configuration: C, environment: Environment) {
        component = DaggerCertificateCheckComponent.builder()
                .certificateCheckModule(CertificateCheckModule(configuration.certificateCheckSettings, environment))
                .build()
    }
}