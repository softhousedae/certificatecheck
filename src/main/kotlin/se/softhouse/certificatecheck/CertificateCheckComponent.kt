package se.softhouse.certificatecheck

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [CertificateCheckModule::class])
interface CertificateCheckComponent {
    fun certificateCheckService(): CertificateCheckService
}