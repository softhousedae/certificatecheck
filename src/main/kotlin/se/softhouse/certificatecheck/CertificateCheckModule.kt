package se.softhouse.certificatecheck

import dagger.Module
import dagger.Provides
import io.dropwizard.client.HttpClientBuilder
import io.dropwizard.setup.Environment
import org.apache.http.HttpRequest
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.RedirectStrategy
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.impl.client.LaxRedirectStrategy
import org.apache.http.protocol.HttpContext
import javax.inject.Named
import javax.inject.Singleton

@Module
class CertificateCheckModule(private val certificateCheckSettings: CertificateCheckSettings, private val environment: Environment) {
    @Provides
    @Named("certificateCheck")
    @Singleton
    fun provideClient(certificateHttpProcessor: CertificateHttpProcessor): HttpClient {
        return HttpClientBuilder(environment)
                .using(certificateCheckSettings.httpClient)
                .using(object : RedirectStrategy {
                    override fun getRedirect(request: HttpRequest?, response: HttpResponse?, context: HttpContext?): HttpUriRequest {
                        error("not implemented")
                    }

                    override fun isRedirected(request: HttpRequest?, response: HttpResponse?, context: HttpContext?): Boolean {
                        return false
                    }

                })
                .using(certificateHttpProcessor)
                .build("certificateCheck")
    }
}
