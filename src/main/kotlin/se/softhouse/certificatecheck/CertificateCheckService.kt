package se.softhouse.certificatecheck

import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.protocol.BasicHttpContext
import java.net.URI
import java.security.cert.X509Certificate
import javax.inject.Inject
import javax.inject.Named

class CertificateCheckService @Inject constructor(@Named("certificateCheck") private val httpClient: HttpClient) {

    fun getCertificate(url: URI): X509Certificate {
        require(url.scheme == "https", { "URL not https" })

        val host = url.host
        val context = BasicHttpContext()
        val request = HttpGet(url)
        request.addHeader("Host", host)
        httpClient.execute(request, context)

        val certificates = CertificateHttpProcessor.getPeerCertificates(context)
        return certificates.first {
            it.subjectAlternativeNames != null
                    && it.subjectAlternativeNames.stream().filter {
                it[0] == HOSTNAME_TYPE && it[1] == host
            }.findFirst().isPresent
        }
    }

    companion object {
        private const val HOSTNAME_TYPE = 2
    }
}