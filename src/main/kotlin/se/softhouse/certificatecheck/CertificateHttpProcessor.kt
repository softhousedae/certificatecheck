package se.softhouse.certificatecheck

import org.apache.http.HttpRequest
import org.apache.http.HttpResponse
import org.apache.http.conn.ManagedHttpClientConnection
import org.apache.http.protocol.HttpContext
import org.apache.http.protocol.HttpCoreContext
import org.apache.http.protocol.HttpProcessor
import java.security.cert.Certificate
import java.security.cert.X509Certificate
import javax.inject.Inject

class CertificateHttpProcessor @Inject constructor() : HttpProcessor {
    override fun process(request: HttpRequest?, context: HttpContext?) {
        // deliberately empty
    }

    override fun process(response: HttpResponse, context: HttpContext) {
        val routedConnection = context.getAttribute(HttpCoreContext.HTTP_CONNECTION) as ManagedHttpClientConnection
        val sslSession = routedConnection.sslSession
        if (sslSession != null) {
            val certificates: Array<Certificate> = sslSession.peerCertificates
            context.setAttribute(PEER_CERTIFICATES, certificates)
        }
    }

    companion object {
        const val PEER_CERTIFICATES = "PEER_CERTIFICATES"

        fun getPeerCertificates(context: HttpContext): Array<X509Certificate> {
            @Suppress("UNCHECKED_CAST")
            return context.getAttribute(CertificateHttpProcessor.PEER_CERTIFICATES) as Array<X509Certificate>
        }
    }
}