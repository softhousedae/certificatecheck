package se.softhouse.certificatecheck

import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import com.github.tomakehurst.wiremock.junit.WireMockRule
import io.dropwizard.client.JerseyClientBuilder
import io.dropwizard.client.JerseyClientConfiguration
import io.dropwizard.testing.ConfigOverride
import io.dropwizard.testing.ResourceHelpers
import io.dropwizard.testing.junit.DropwizardAppRule
import io.dropwizard.util.Duration
import org.assertj.core.api.Assertions.assertThat
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import java.net.URI
import java.util.*
import javax.ws.rs.client.Entity


class IntegrationTestCertificateOK {

    companion object {

        private const val VALIDITY_OK = 31

        @JvmField
        @ClassRule
        var keystore = TemporaryKeystore(VALIDITY_OK)

        @JvmField
        @ClassRule
        val rule = DropwizardAppRule<TestConfig>(
                TestApp::class.java,
                ResourceHelpers.resourceFilePath("IntegrationTest.yml"),
                ConfigOverride.config("certificateCheck.httpClient.tls.trustStorePath", keystore.path))
    }

    @Rule
    @JvmField
    var wireMockRule = WireMockRule(
            wireMockConfig()
                    .dynamicPort()
                    .dynamicHttpsPort()
                    .keystorePath(keystore.path)
    )

    @Test
    fun testSiteWithCertificateOk() {
        // given
        stubFor(get(urlEqualTo("/")).willReturn(aResponse()))

        val jerseyConfig = JerseyClientConfiguration()
        jerseyConfig.connectionTimeout = Duration.seconds(10)
        jerseyConfig.timeout = Duration.seconds(10)
        jerseyConfig.isGzipEnabled = false
        val client = JerseyClientBuilder(rule.environment)
                .using(jerseyConfig)
                .build("testSiteWithCertificateOk")

        // when
        val response = client.target("http://127.0.0.1:${rule.localPort}/test")
                .request()
                .post(Entity.json(CertificateModel(url = URI.create("https://localhost:${wireMockRule.httpsPort()}"))))
        val data = response.readEntity(CertificateModel::class.java)
        println(data)

        // then
        assertThat(response.status).isEqualTo(200)

        assertThat(data.notBefore).isBefore(data.notAfter)
        assertThat(data.notBefore).isBefore(Date())
        assertThat(data.notAfter).isAfter(Date())
    }

}

