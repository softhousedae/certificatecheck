package se.softhouse.certificatecheck

import org.junit.rules.ExternalResource
import org.junit.rules.TemporaryFolder
import java.io.File


class TemporaryKeystore(validity: Int) : ExternalResource() {
    private val folder = TemporaryFolder()
    val path: String

    init {
        folder.create()
        path = File(folder.newFolder(), "keystore.jks").toString()
        sun.security.tools.keytool.Main.main(arrayOf(
                "-genkey", "-dname", "CN=localhost, OU=Unknown, O=Unknown, L=Unknown, ST=Unknown, C=Unknown", "-alias", "wiremock", "-keyalg", "RSA", "-keysize", "1024", "-validity", validity.toString(), "-keypass", "password", "-keystore", path, "-storepass", "password", "-ext", "SAN=dns:localhost"
        ))
    }

    override fun after() {
        folder.delete()
    }
}

