package se.softhouse.certificatecheck

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule
import dagger.Component
import dagger.Module
import dagger.Provides
import io.dropwizard.Application
import io.dropwizard.Configuration
import io.dropwizard.ConfiguredBundle
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import java.net.URI
import java.util.*
import javax.inject.Inject
import javax.inject.Scope
import javax.validation.Valid
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class TestAppScope

@Module
class TestModule(private val environment: Environment) {
    @Provides
    @TestAppScope
    fun provideEnvironment() = environment
}

@TestAppScope
@Component(modules = [TestModule::class], dependencies = [CertificateCheckComponent::class])
interface TestComponent {
    fun testResource(): TestResource
}


class TestConfig : Configuration(), CertificateCheckConfig {
    @Valid
    @JsonProperty("certificateCheck")
    override var certificateCheckSettings: CertificateCheckSettings = CertificateCheckSettings()

    // ...
}

data class CertificateModel(val url: URI, val notBefore: Date? = null, val notAfter: Date? = null)


@Path("/test")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
class TestResource @Inject constructor(private val certificateCheckService: CertificateCheckService) {
    @POST
    fun test(certificateModel: CertificateModel): CertificateModel {
        val certificate = certificateCheckService.getCertificate(certificateModel.url)
        return CertificateModel(url = certificateModel.url, notBefore = certificate.notBefore, notAfter = certificate.notAfter)
    }

}

class TestApp : Application<TestConfig>() {
    private lateinit var component: TestComponent

    override fun initialize(bootstrap: Bootstrap<TestConfig>) {
        val certificateCheckBundle = CertificateCheckBundle<TestConfig>()
        bootstrap.addBundle(certificateCheckBundle)

        bootstrap.addBundle(object : ConfiguredBundle<TestConfig> {

            override fun run(configuration: TestConfig, environment: Environment) {
                component = DaggerTestComponent.builder()
                        .certificateCheckComponent(certificateCheckBundle.component)
                        .build()
            }

            override fun initialize(bootstrap: Bootstrap<*>) {}
        })
    }

    override fun run(configuration: TestConfig, environment: Environment) {
        environment.objectMapper.apply {
            configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            registerModule(KotlinModule())
        }
        environment.jersey().register(component.testResource())
    }

}
